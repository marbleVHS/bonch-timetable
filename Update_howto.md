# Как обновлять прогу между семестрами

1. Обновить поле `ACTUAL_SEMESTER` в `StartupActivity`;
2. Проставить дату начала семестра в `SutTimetable`;
3. Обновить `versionCode` и `versionName` в `build.gradle`;
4. Опционально: добавить сокращения в `assets/shortens-sut.json`
5. Закоммитить с именем формата `Version: x.y.z: Big change name`
6. Добавить тэг на гитлабе
7. Залить на гуглоплей
