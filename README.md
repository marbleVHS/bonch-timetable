# Bonch Timetable | Бонч.Расписание 

Позволяет стянуть расписание с сайта Бонча и записать в штатное приложение календаря.

[Google Play](https://play.google.com/store/apps/details?id=com.iillyyaa2033.timetablefetcher)

![](https://gitlab.com/iria_somobu/what-ive-done/-/raw/master/imgs/bonch-timetable.png)



## Структура проекта
```
app/        Android-приложуха для гуглоплей
legal/      Privacy policy для гуглоплей
parser/     Либа для парсинга расписания
prepod/     Тулза для парсинга расписания преподов
```

# Лицензия

[GNU GPL v3](https://gitlab.com/iria_somobu/bonch-timetable/-/blob/master/LICENSE.md)
