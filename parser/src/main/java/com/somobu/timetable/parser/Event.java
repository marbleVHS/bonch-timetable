package com.somobu.timetable.parser;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

/**
 * Событие, которое будет записано в календарь или отображено в списке расписания.
 */
public class Event implements Comparable<Event> {

    public final long id;
    public final long calendarId;
    public final String title;
    public final long start;

    // Optional data
    public long duration = 0;
    public String description = "";
    public String shortDescr = "";
    public String location = "";
    public String pair = "";

    public boolean isOngoingNow = false;

    public Event(long id, long calendar, String title, long start) {
        this.title = title;
        this.start = start;
        this.calendarId = calendar;
        this.id = id;
    }

    public Event(Pair pair, Map<String, String> shortens, long start) {
        this(-1, -1, pair.getTitle(shortens), start);
        this.duration = pair.getDuration();
        this.description = pair.getDesctiption();
        this.shortDescr = pair.getShortDescription();
        this.location = pair.getLocation();

        if (pair.getPair() < 10) {
            this.pair = "" + pair.getPair();
        }
    }

    @Override
    public String toString() {
        String formattedStart = new SimpleDateFormat("yyyy.MM.dd hh:mm", Locale.getDefault()).format(new Date(start));
        return "#" + id + " " + title + " " + formattedStart;
    }

    public boolean atSameDate(Event event) {
        return event.start == start;
    }

    public boolean atSameDay(Event curr) {
        @SuppressWarnings("SimpleDateFormat") SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
        return fmt.format(new Date(curr.start)).equals(fmt.format(new Date(start)));
    }

    @Override
    public int compareTo(Event o) {
        if (o == null) return 1;
        return (int) (start - o.start);
    }
}
