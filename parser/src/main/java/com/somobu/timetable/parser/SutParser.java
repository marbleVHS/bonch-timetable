package com.somobu.timetable.parser;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Бончовое расписание.
 *
 * <p>
 * Использовать следующим образом: создаешь, вызваешь parse(), затем получаешь нужные события через
 * getEventsAt()
 * </p>
 */
public class SutParser {

    private static final boolean DEBUG_CONNECT = false;
    private static final boolean DEBUG_PARSE = false;

    /**
     * URL расписания на случай, если иной не указан. Также - пример URL'ки расписания
     */
    private static final String HARDCODED_RQ = "https://cabinet.sut.ru/raspisanie_all_new"
            + "?schet=205.1819%2F2" + "&type_z=1" + "&faculty=50005" + "&kurs=1" + "&group=53808"
            + "&ok=%D0%9F%D0%BE%D0%BA%D0%B0%D0%B7%D0%B0%D1%82%D1%8C" + "&group_el=0";

    public static final HashMap<String, Long> times = new HashMap<>();

    static {
        // Это захардкоженные начала семестров СПбГУТ
        //
        // Начало семетра - это время в миллисекундах, 00:00 понедельника (по локальному времени) первой учебной недели.
        //
        // К сожалению, сейчас нужно забивать эти даты ручками каждый сем -- в отсутствие API других вариантов нет.
        // Никто не гарантирует, что начало следующего семестра будет соответствовать нашим рассчетам на основе
        // закономерности прошлых лет. Как обычно - спасибо, Бонч!

        times.put("205.1819/1", 1535922000000L);
        times.put("205.1819/2", 1549832400000L);
        times.put("205.1920/1", 1567371600000L);
        times.put("205.1920/2", 1580677200000L + 7 * 24 * 60 * 60 * 1000);
        times.put("205.2021/1", 1598821200000L);
        times.put("205.2021/2", 1612731600000L);
        times.put("205.2122/1", 1630270800000L);
        times.put("205.2122/2", 1644181200000L);
        times.put("205.2223/1", 1661720400000L);
    }

    private final String targetUrl;
    private final Map<String, String> shortens;

    public ArrayList<Pair> parsedPairs;
    public String semesterName = null;
    public String groupName = null;
    private long semesterStart;

    private volatile boolean isPrepared = false;

    public SutParser(String targetUrl, Map<String, String> shortens) {
        this.targetUrl = targetUrl;
        this.shortens = shortens;

        parsedPairs = new ArrayList<>();
    }

    /**
     * Скачивает и подготавливает расписание. Блокирующий метод
     *
     * @throws IOException              если проблемы с сетью
     * @throws NumberFormatException    если проблемы с цифрами (вероятно, url-ка поменялась)
     * @throws SemesterStartTimeUnknown если начало семестра неизвестно (см. static блок выше)
     * @see #times - мапа начал семестров
     */
    public synchronized void parse() throws IOException, NumberFormatException, SemesterStartTimeUnknown {

        // Clear old timetable
        parsedPairs.clear();

        if (DEBUG_CONNECT) v("Started...");

        String rq = targetUrl;
        if (rq == null) rq = HARDCODED_RQ;

        System.out.println("RQ " + rq);

        Connection conn = Jsoup.connect(rq).userAgent("Mozilla/5.0").timeout(180 * 1000).method(Connection.Method.POST);

        String key = rq.substring(rq.indexOf("schet=") + "schet=".length());
        int idx = key.indexOf("&");
        key = key.substring(0, idx > 0 ? idx : key.length());
        key = key.replace("%2F", "/");

        if (DEBUG_CONNECT) System.out.println("Got key " + key);
        Long rawKey = times.get(key);
        if (rawKey == null) throw new SemesterStartTimeUnknown();
        else semesterStart = rawKey;

        Connection.Response response = conn.execute();
        if (DEBUG_CONNECT) v("Connected...");

        Document document = response.parse();
        extractMeta(rq, document);

        Elements pairs = document.getElementsByClass("pair");

        // Parse result
        for (Element el : pairs) {
            if (DEBUG_PARSE) v(el.html());

            String weekday = el.attr("weekday");
            String pair = el.attr("pair");
            String subject = el.getElementsByClass("subect").get(0).text();
            if (subject.trim().isEmpty()) subject = "Пара не указана";

            String type = el.getElementsByClass("type").get(0).text();
            String weeks = el.getElementsByClass("weeks").get(0).text();

            Elements teacherEl = el.getElementsByClass("teacher");
            String teacher;
            if (teacherEl.size() > 0) {
                if (teacherEl.get(0).hasAttr("title")) {
                    teacher = teacherEl.get(0).attr("title");
                } else {
                    teacher = teacherEl.get(0).text();
                }
                teacher = teacher.replace(";", "");
            } else {
                teacher = "Препод не указан";
            }

            Elements audEls = el.getElementsByClass("aud");
            String aud;
            if (audEls.size() > 0) {
                aud = audEls.get(0).text();
            } else {
                aud = "      Аудитория не указана"; // Имя аудитории будет урезано на первые шесть символов. Да, костыль
            }

            // Скипаем военку
            if (subject.equalsIgnoreCase("военная подготовка")) continue;

            parsedPairs.add(Pair.parse(weekday, pair, subject, type, weeks, teacher, aud));
        }

        if (DEBUG_PARSE) {
            v("Parsed:");

            for (Pair p : parsedPairs) {
                v(p.toString());
            }
        }

        isPrepared = true;
    }

    private void extractMeta(String rq, Document document) {

        String schet = rq.substring(rq.indexOf("schet=") + "schet=".length());
        int schet_idx = schet.indexOf("&");
        schet = schet.substring(0, schet_idx > 0 ? schet_idx : schet.length());
        schet = schet.replace("%2F", "/");
        semesterName = document.getElementById("schet").getElementsByAttributeValue("value", schet).first().text();

        System.out.println("Sem/group: " + semesterName + " " + groupName);
    }

    /**
     * Забираем события за определенный период
     *
     * @param bounds временные рамки
     * @return список событий в заданных рамках
     * @throws RuntimeException если забыл вызвать prepare()
     * @see #parse()
     */
    public ArrayList<Event> getEventsAt(DateSpan bounds) throws RuntimeException {
        if (!isPrepared) throw new RuntimeException("Pairs not prepared yet");

        ArrayList<Event> events = new ArrayList<>();

        for (Pair pair : parsedPairs) {
            for (long start : pair.getAllStarts(semesterStart)) {
                if (bounds.contains(start)) {
                    events.add(new Event(pair, shortens, start));
                }
            }
        }

        return events;
    }

    private void v(String s) {
        System.out.println(s);
    }

    public static class SemesterStartTimeUnknown extends Exception {

        public SemesterStartTimeUnknown() {
            super("Semester start time unknown");
        }

    }
}
