package com.somobu.timetable.parser;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Некоторый диапазон дат.
 */
public class DateSpan {

    public final String startText, endText;
    public final long startMillis, endMillis;

    private DateSpan(long start, long end) {
        this.startMillis = start;
        this.endMillis = end;

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        startText = df.format(new Date(start));
        endText = df.format(new Date(end));
    }

    public static DateSpan currentAndNextWeek() {
        // Get calendar set to current date and time
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0); // ! clear would not reset the hour of day !
        c.clear(Calendar.MINUTE);
        c.clear(Calendar.SECOND);
        c.clear(Calendar.MILLISECOND);

        // Set the calendar to first day of the current week
        c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek());

        long startMillis = c.getTimeInMillis();
        c.add(Calendar.WEEK_OF_YEAR, 2);
        long endMillis = c.getTimeInMillis();

        return new DateSpan(startMillis, endMillis);
    }

    public static DateSpan allAvailable() {
        return new DateSpan(0, Long.MAX_VALUE);
    }

    public boolean contains(long timeMillis) {
        return timeMillis >= startMillis && timeMillis <= endMillis;
    }

}
