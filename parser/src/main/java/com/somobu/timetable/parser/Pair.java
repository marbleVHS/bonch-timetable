package com.somobu.timetable.parser;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Бончовая пара.
 *
 * <p>
 * Пара - это занятие, которое может происходить на одной или нескольких неделях в определенное (заданное) время.
 * Например, по понедельникам в девять утра.
 * </p><p>
 * Если по-простому, то пара - это отдельное занятие в одном (и только одном) квадрате расписания
 * <a href="https://cabinet.sut.ru/raspisanie_all_new">на страничке расписания</a>.
 * </p>
 */
public class Pair {

    private final int[] weeks;
    private final int weekday;
    private final int pair;

    private final String subject;
    private final String type;
    public final String teacher;
    private final String aud;

    private Pair(int weekday, int pair, String subject, String type, int[] weeks, String teacher, String aud) {
        this.weekday = weekday;
        this.pair = pair;
        this.subject = subject;
        this.type = type;
        this.weeks = weeks;
        this.teacher = teacher;
        this.aud = aud;
    }

    static Pair parse(String weekday, String pair, String subject, String type, String weeks, String teacher, String aud) throws NumberFormatException {

        int pWeekday = Integer.parseInt(weekday);
        int pPair = Integer.parseInt(pair);
        int[] pWeeks = parseDirtyWeekNumbers(weeks);

        return new Pair(pWeekday, pPair, subject, type, pWeeks, teacher, aud.substring(6));
    }

    private static int[] parseDirtyWeekNumbers(String data) {
        String clearData = data.replaceAll("[^\\d\\s]", "");
        String[] stringArray = clearData.split(" ");
        int[] output = new int[stringArray.length];

        for (int i = 0; i < output.length; i++) {
            if (stringArray[i].trim().isEmpty()) {
                output[i] = -11;
            }

            try {
                output[i] = Integer.parseInt(stringArray[i]);
            } catch (Exception e) {
                System.err.println("Error while parsing dirty array \"" + clearData + "\":\n\t" + e);
            }
        }

        return output;
    }

    /**
     * Converts array with week numbers to string, for debug purpose
     */
    private static String weeksArrayToString(int[] array) {
        StringBuilder result = new StringBuilder();

        for (int value : array) {
            result.append(value).append("|");
        }

        if (result.length() > 0)
            result = new StringBuilder(result.substring(0, result.length() - 1));

        return result.toString();
    }

    private static long asMillis(long semesterStart, int week, int weekday, int pair) {
        long result = semesterStart;

        long millisInDay = 1000 * 60 * 60 * 24;
        long millisInWeek = millisInDay * 7;

        result += (week - 1) * millisInWeek;
        result += (weekday - 1) * millisInDay;
        result += pairInMillisOffset(pair);

        return result;
    }

    /**
     * По какой-то причине во втором семестре 18/19 учебного года
     * айдишники номеров регулярных пар (1-5) смещены на единицу вниз (т.е. 2-6).
     * <br/><br/>
     * Это поле - костыль на случай, если регулярные пары опять куда-то
     * сместятся.
     * <br/><br/>
     * Примечательно, что на физкультуры (83-87) это правило не распространяется
     */
    private static final int regularPairOffset = 1;

    private static long pairInMillisOffset(int pair) {

        switch (pair) {
            case 1 + regularPairOffset:
                return timeOffset(9, 0);
            case 2 + regularPairOffset:
                return timeOffset(10, 45);
            case 3 + regularPairOffset:
                return timeOffset(13, 0);
            case 4 + regularPairOffset:
                return timeOffset(14, 45);
            case 5 + regularPairOffset:
                return timeOffset(16, 30);
            case 6 + regularPairOffset:
                return timeOffset(18, 15);
            case 30:
                return timeOffset(20, 0);
            case 83:
                return timeOffset(9, 0); // Обычно это физкультуры
            case 84:
                return timeOffset(10, 30);
            case 85:
                return timeOffset(12, 0);
            case 86:
                return timeOffset(13, 30);
            case 87:
                return timeOffset(15, 0);
            default:
                System.out.println("No offset for " + (pair));
                return 0;
        }
    }

    private static long timeOffset(int hour, int minutes) {
        return hour * 60 * 60 * 1000 + minutes * 60 * 1000;
    }

    private static long durationByPairNumber(int pair) {
        switch (pair) {
            case 83:
            case 84:
            case 85:
            case 86:
            case 87:
                return timeOffset(1, 30);
            default:
                return timeOffset(1, 35);
        }
    }

    private static String abbrSubject(Map<String, String> shortens, String subj) {
        if (shortens.containsKey(subj.toLowerCase())) {
            return shortens.get(subj.toLowerCase());
        } else {
            System.out.println("No shorten for [" + subj.toLowerCase() + "]");
            return subj;
        }
    }

    private static String abbrType(String type) {
        switch (type.toLowerCase()) {
            case "(лекция)":
                return "лк";
            case "(практические занятия)":
                return "пр";
            case "(лабораторная работа)":
                return "лаб";
            case "()":
                return "";
            default:
                return type;
        }
    }

    private static String abbrAddr(String location) {
        return location
                .replace("-Б22", "")
                .replace("; Б22", "");
    }


    @Override
    public String toString() {
        return weekday + "/" + pair + " - " + subject + " " + type + " - " + teacher + " " + aud + " [" + weeksArrayToString(weeks) + "]";
    }


    public String getTitle() {
        return getTitle(new HashMap<>());
    }

    String getTitle(Map<String, String> shortens) {
        return Pair.abbrSubject(shortens, subject) + " " + Pair.abbrType(type);
    }

    public String getShortDescription() {
        return "Преподаватель: " + teacher;
    }

    public String getDesctiption() {
        return "Преподаватель: " + teacher +
                "\n " +
                "\nПолное название предмета: " + subject;
    }

    public String getLocation() {
        return Pair.abbrAddr(aud);
    }

    public long[] getAllStarts(long semesterStart) {
        long[] result = new long[weeks.length];

        for (int i = 0; i < result.length; i++) {
            result[i] = asMillis(semesterStart, weeks[i], weekday, pair);
        }

        return result;
    }

    public long getDuration() {
        return Pair.durationByPairNumber(pair);
    }

    public int getPair() {
        return pair;
    }
}
