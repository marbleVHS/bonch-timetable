const delay = ms => new Promise(res => setTimeout(res, ms));

let faculty = document.querySelector("#faculty")
let kurs = document.querySelector("#kurs")
let group = document.querySelector("#group")


let result = {}

const fetch = async () => {

  for(fac of faculty.children) { 
    if(fac.value == 0) continue;

    faculty.value = fac.value
    change_fac(1);
    console.log("Changing fac to "+fac.value)

    facDict = {}
    await delay(1000)

    for(kr of kurs.children){
      if(kr.value === "") continue;
      
      kurs.value = kr.value
      change_fac(0);
      console.log("Changing kurs to "+kr.value)

      krList = []     
      await delay(1000)

      for(gr of group.children){
        if(gr.value === "") continue;
        krList.push(gr.value)
      }

      facDict[kr.value] = krList;
    }

    result[fac.value] = facDict;
    
  }

}

fetch().then( () => console.log(JSON.stringify(result)) )
