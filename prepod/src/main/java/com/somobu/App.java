package com.somobu;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.somobu.timetable.parser.Pair;
import com.somobu.timetable.parser.SutParser;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class App {

    static HashMap<String, ArrayList<Pair>> pairsByPrepod = new HashMap<>();

    static String fmt = "https://cabinet.sut.ru/raspisanie_all_new"
            + "?schet=%s" + "&type_z=%s" + "&faculty=%s" + "&kurs=%s" + "&group=%s"
            + "&ok=%s" + "&group_el=0";

    static String schet = "205.2223/1"; // Семестр (осень 2022)
    static String type_z = "1"; // Тип расписания (занятия)
    static String ok = "%D0%9F%D0%BE%D0%BA%D0%B0%D0%B7%D0%B0%D1%82%D1%8C"; // Оставь как есть

    static class GroupsJson extends HashMap<String, HashMap<String, ArrayList<String>>> {
        // Nothing here, this is just a typedef-like convenience class
    }

    public static void main(String[] args) throws IOException, SutParser.SemesterStartTimeUnknown {

        File folder = new File("prepod/parsed");
        if (!folder.exists() && !folder.mkdirs()) throw new RuntimeException();

        GroupsJson idx = readGroups();

        fetchTimetables(idx);

        writePrepods(folder);
        writeIndex();
    }

    static GroupsJson readGroups() throws IOException {
        String resourceName = "prepod/groups.json";
        InputStream is = Files.newInputStream(new File(resourceName).toPath());
        Scanner s = new Scanner(is).useDelimiter("\\A");
        String result = s.hasNext() ? s.next() : "";

        Gson gson = new GsonBuilder().create();
        return gson.fromJson(result, GroupsJson.class);
    }

    static void fetchTimetables(GroupsJson idx) throws SutParser.SemesterStartTimeUnknown, IOException {
        for (String faculty : idx.keySet()) {
            HashMap<String, ArrayList<String>> facultyObj = idx.get(faculty);

            for (String kurs : facultyObj.keySet()) {
                ArrayList<String> kursArr = facultyObj.get(kurs);

                for (String group : kursArr) {

                    String url = String.format(fmt, schet, type_z, faculty, kurs, group, ok);
                    System.out.println(url);

                    SutParser parser = new SutParser(url, new HashMap<>());
                    parser.parse();

                    for (Pair pair : parser.parsedPairs) {
                        String teacher = pair.teacher.trim();
                        if (!pairsByPrepod.containsKey(teacher)) {
                            pairsByPrepod.put(teacher, new ArrayList<>());
                        }

                        pairsByPrepod.get(teacher).add(pair);
                    }
                }
            }
        }
    }

    static void writePrepods(File folder) throws IOException {
        Gson gson = new GsonBuilder().create();

        for (String key : pairsByPrepod.keySet()) {
            ArrayList<Pair> list = pairsByPrepod.get(key);

            File file = new File(folder, key + ".json");
            try {
                file.delete();
            } catch (Exception ignored) {

            }

            file.createNewFile();

            String rz = gson.toJson(list);

            FileOutputStream outputStream = new FileOutputStream(file);
            byte[] strToBytes = rz.getBytes();
            outputStream.write(strToBytes);
            outputStream.close();
        }
    }

    static void writeIndex() throws IOException {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        File folder = new File("prepod/parsed");
        File file = new File(folder, "index.json");
        file.delete();

        String[] items = folder.list();
        Arrays.sort(items);

        file.createNewFile();

        String rz = gson.toJson(items);

        FileOutputStream outputStream = new FileOutputStream(file);
        byte[] strToBytes = rz.getBytes();
        outputStream.write(strToBytes);
        outputStream.close();
    }
}
