package com.somobu.timetable.timetables

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.TextView
import androidx.annotation.Keep
import androidx.fragment.app.viewModels
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.iillyyaa2033.timetablefetcher.R
import com.somobu.timetable.GLHelper

class TeachersListFragment : androidx.fragment.app.Fragment() {

    private class IndexData {
        var isLoaded = false
        var e: java.lang.Exception? = null
        var index: Array<String>? = null
    }

    private class CrappyVM(application: android.app.Application) : AndroidViewModel(application) {

        private val indexData: MutableLiveData<IndexData> by lazy {
            MutableLiveData(IndexData()).also {
                loadIndex()
            }
        }

        fun index(): LiveData<IndexData> {
            return indexData
        }

        fun loadIndex() {
            object : Thread() {
                @Keep
                override fun run() {
                    try {
                        val result = GLHelper.get("index.json")

                        val gson: Gson = GsonBuilder().create()
                        val token: java.lang.reflect.Type = TypeToken.getParameterized(java.util.ArrayList::class.java, String::class.java).getType()
                        val rz: java.util.ArrayList<String> = gson.fromJson(result, token)

                        val index: Array<String> = rz.toTypedArray()

                        val id = IndexData()
                        id.isLoaded = true
                        id.index = index
                        indexData.postValue(id)

                    } catch (e: java.lang.Exception) {
                        val id = IndexData()
                        id.isLoaded = true
                        id.e = e
                        indexData.postValue(id)
                    }
                }
            }.start()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_prepod_chooser, container, false)
    }

    override fun onResume() {
        super.onResume()

        val cvm: CrappyVM by viewModels()
        cvm.index().observe(this, { index: IndexData ->
            if (index.isLoaded) {
                if (index.index != null) onIndexLoaded(index.index!!)
                if (index.e != null) onError(index.e!!)
            } else {
                setPleaseWait()
            }
        })
    }

    private fun onError(e: java.lang.Exception) {
        e.printStackTrace()
        setErrorRetry()

        val root: View = getView() ?: return

        (root.findViewById<View>(R.id.textView4) as TextView).text = e.localizedMessage

        root.findViewById<View>(R.id.button).setOnClickListener({
            val cvm: CrappyVM by viewModels()
            cvm.loadIndex()
        })
    }

    private fun onIndexLoaded(index: Array<String>) {
        setPrepodList()
        val root: View = getView() ?: return

        val tv = root.findViewById<AutoCompleteTextView>(R.id.autoCompleteTextView)
        val adapter = MyAdapter(requireContext(), android.R.layout.simple_list_item_1, index)

        tv.setAdapter(adapter)
        tv.setOnItemClickListener { _, _, i, _ ->
            val bundle = Bundle()
            bundle.putString("teacher", adapter.getSuperItem(i))
            findNavController().navigate(R.id.action_teachersListFragment_to_teacherTtFragment, bundle)
            tv.setText("");
        }
    }

    private fun setPleaseWait() {
        val root: View = getView() ?: return
        root.findViewById<View>(R.id.please_wait).setVisibility(View.VISIBLE)
        root.findViewById<View>(R.id.error_retry).setVisibility(View.GONE)
        root.findViewById<View>(R.id.prepod_list).setVisibility(View.GONE)
    }

    private fun setErrorRetry() {
        val root: View = getView() ?: return
        root.findViewById<View>(R.id.please_wait).visibility = View.GONE
        root.findViewById<View>(R.id.error_retry).visibility = View.VISIBLE
        root.findViewById<View>(R.id.prepod_list).visibility = View.GONE
    }

    private fun setPrepodList() {
        val root: View = getView() ?: return
        root.findViewById<View>(R.id.please_wait).visibility = View.GONE
        root.findViewById<View>(R.id.error_retry).visibility = View.GONE
        root.findViewById<View>(R.id.prepod_list).visibility = View.VISIBLE
    }

    internal class MyAdapter(context: android.content.Context, resource: Int, objects: Array<String>) : ArrayAdapter<String?>(context, resource, objects) {

        override fun getItem(position: Int): String {
            return super.getItem(position)!!.replace(".json", "")
        }

        fun getSuperItem(p: Int): String {
            return super.getItem(p)!!
        }
    }
}