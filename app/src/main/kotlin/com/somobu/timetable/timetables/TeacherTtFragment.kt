package com.somobu.timetable.timetables

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import androidx.annotation.Keep
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.iillyyaa2033.timetablefetcher.R
import com.iillyyaa2033.timetablefetcher.StartupActivity
import com.somobu.timetable.GLHelper
import com.somobu.timetable.parser.DateSpan
import com.somobu.timetable.parser.Event
import com.somobu.timetable.parser.Pair
import com.somobu.timetable.parser.SutParser
import java.util.*
import kotlin.collections.HashMap

class TeacherTtFragment : Fragment() {

    private class TimetableData(val teacher: String?) {
        var isLoaded = false
        var e: Exception? = null
        var index: ArrayList<Event>? = null
    }

    private class CrappyVM(application: Application?) : AndroidViewModel(application!!) {

        private var indexData: MutableLiveData<TimetableData>? = null

        fun timetable(tt: String): LiveData<TimetableData> {
            if (indexData == null) {
                indexData = MutableLiveData(TimetableData(tt))
                loadTimetable(tt)
            } else if (tt != indexData!!.value!!.teacher) {
                loadTimetable(tt)
            }
            return indexData!!
        }

        fun loadTimetable(prepod: String) {

            object : Thread() {

                @Keep
                override fun run() {
                    try {
                        indexData!!.postValue(TimetableData(prepod))

                        val string = GLHelper.get(prepod);

                        val gson = GsonBuilder().create()
                        val token = TypeToken.getParameterized(ArrayList::class.java, Pair::class.java).type
                        val rz = gson.fromJson<ArrayList<Pair>>(string, token)
                        val pairs = rz.toTypedArray()
                        val events = getEventsAt(pairs, DateSpan.currentAndNextWeek())

                        val data = TimetableData(prepod)
                        data.isLoaded = true
                        data.index = events
                        indexData!!.postValue(data)
                    } catch (e: Exception) {
                        val data = TimetableData(prepod)
                        data.isLoaded = true
                        data.e = e
                        indexData!!.postValue(data)
                    }
                }

                @Keep
                @Throws(RuntimeException::class)
                fun getEventsAt(pairs: Array<Pair>, bounds: DateSpan): ArrayList<Event> {
                    val semesterStart = SutParser.times["205." + StartupActivity.ACTUAL_SEMESTER.replace(".", "")]!!
                    val events = ArrayList<Event>()
                    val shortens = HashMap<String, String>()

                    for (pair in pairs) {
                        for (start in pair.getAllStarts(semesterStart)) {
                            if (bounds.contains(start)) {
                                events.add(Event(pair, shortens, start))
                            }
                        }
                    }

                    events.sort()

                    return events
                }

            }.start()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val prepod = requireArguments().getString("teacher")

        val cvm: CrappyVM by viewModels()
        cvm.timetable(prepod!!).observe(this) { index: TimetableData ->
            if (index.isLoaded) {
                if (index.index != null) onEventsLoaded(index.index)
                if (index.e != null) onError(prepod, index.e)
            } else {
                setPleaseWait()
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_prepod_tt, container, false)
    }

    private fun setPleaseWait() {
        val root = view ?: return
        root.findViewById<View>(R.id.please_wait).visibility = View.VISIBLE
        root.findViewById<View>(R.id.error_retry).visibility = View.GONE
        root.findViewById<View>(R.id.prepod_list).visibility = View.GONE
    }

    private fun onEventsLoaded(events: ArrayList<Event>?) {
        val root = view ?: return
        root.findViewById<View>(R.id.please_wait).visibility = View.GONE
        root.findViewById<View>(R.id.error_retry).visibility = View.GONE
        root.findViewById<View>(R.id.prepod_list).visibility = View.VISIBLE
        val adapter: ArrayAdapter<Event> = EventsAdapter(requireContext(), events!!.toTypedArray())
        val listView = root.findViewById<ListView>(R.id.list)
        listView.adapter = adapter
    }

    private fun onError(preopd: String, e: Exception?) {
        e!!.printStackTrace()

        val root = view ?: return
        root.findViewById<View>(R.id.please_wait).visibility = View.GONE
        root.findViewById<View>(R.id.error_retry).visibility = View.VISIBLE
        root.findViewById<View>(R.id.prepod_list).visibility = View.GONE
        (root.findViewById<View>(R.id.textView4) as TextView).text = e.localizedMessage

        root.findViewById<View>(R.id.button).setOnClickListener {
            val cvm: CrappyVM by viewModels()
            cvm.loadTimetable(preopd)
        }
    }
}
