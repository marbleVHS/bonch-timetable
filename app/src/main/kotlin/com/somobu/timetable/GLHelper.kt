package com.somobu.timetable

import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.URL
import java.net.URLEncoder
import javax.net.ssl.HttpsURLConnection

object GLHelper {

    fun get(file: String): String {
        val fmt = "https://gitlab.com/iria_somobu/bonch-timetable/-/raw/master/prepod/parsed/%s"
        val query = URLEncoder.encode(file, "utf-8").replace("+", "%20")
        val url = URL(String.format(fmt, query))

        val connection = url.openConnection() as HttpsURLConnection
        connection.addRequestProperty("User-Agent", "Mozilla/5.0")
        connection.connect()

        val reader = BufferedReader(InputStreamReader(connection.inputStream))
        val buffer = StringBuffer()
        var line: String?
        while (reader.readLine().also { line = it } != null) buffer.append(line)

        connection.disconnect()

        return buffer.toString();
    }

}