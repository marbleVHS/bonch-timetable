package com.iillyyaa2033.timetablefetcher;

import android.app.Activity;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.widget.TextView;

public class ViewEventActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_event);

        ((TextView)findViewById(R.id.info)).setText(getIntent().getStringExtra(CalendarContract.EXTRA_CUSTOM_APP_URI));

        findViewById(R.id.close_activity).setOnClickListener(v -> {
            setResult(Activity.RESULT_OK);
            finish();
        });
    }
}
