package com.iillyyaa2033.timetablefetcher;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import com.somobu.timetable.MainActivity;

public class StartupActivity extends Activity {

    /**
     * Это - ключ SharedPref'ов, под которым мы храним код последнего перевыбора семестра.
     *
     * <p>
     * Перевыбор семестра нужен для того, чтобы уведомить юзера о собственно смене сема, чтобы
     * юзер ручками выбрал свой новый факультет, курс и группу.
     * </p>
     *
     * @see #ACTUAL_SEMESTER, в котором записан текущий код выбранного семестра
     */
    public static final String UPDATED_SEMESTER_KEY = "updated_semester";

    /**
     * Код актуального семестра в формате год_начала.год_окончания/семестр
     */
    public static final String ACTUAL_SEMESTER = "22.23/1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String query = prefs.getString(MainActivity.SELECTED_QUERY_KEY, null);

        if (query == null) {
            new AlertDialog.Builder(this)
                    .setTitle("Выбор расписания")
                    .setMessage("Чтобы начать работу, необходимо выбрать курс и группу" +
                            "\n\nВ дальнейшем выбор можно будет изменить в настройках.")
                    .setPositiveButton("Далее", (dialog, which) -> Utils.showBonchSettings(StartupActivity.this, url -> {
                        prefs.edit()
                                .putString(MainActivity.SELECTED_QUERY_KEY, url)
                                .putString(UPDATED_SEMESTER_KEY, ACTUAL_SEMESTER)
                                .apply();

                        openMainActivity();
                    }))
                    .setNegativeButton("Выход", (dialog, which) -> performExit())
                    .setCancelable(false)
                    .show();
        } else {
            String sem = prefs.getString(UPDATED_SEMESTER_KEY, "none");

            if (sem != null && sem.equals(ACTUAL_SEMESTER)) {
                openMainActivity();
            } else {
                new AlertDialog.Builder(this)
                        .setTitle("Выбор расписания")
                        .setMessage("Семестр сменился, программа обновлена, поэтому необходимо выбрать расписание заново." +
                                "\n\nВ дальнейшем выбор можно будет изменить.")
                        .setPositiveButton("Далее", (dialog, which) -> Utils.showBonchSettings(StartupActivity.this, url -> {
                            prefs.edit().putString(MainActivity.SELECTED_QUERY_KEY, url).apply();
                            openMainActivity();
                        }))
                        .setNegativeButton("Не важно", (dialog, which) -> openMainActivity())
                        .setCancelable(false)
                        .show();


                prefs.edit().putString(UPDATED_SEMESTER_KEY, ACTUAL_SEMESTER).apply();
            }
        }
    }

    private void performExit() {
        finish();
        System.exit(1);
    }

    private void openMainActivity() {
        startActivity(new Intent(StartupActivity.this, MainActivity.class));
        finish();
    }
}
