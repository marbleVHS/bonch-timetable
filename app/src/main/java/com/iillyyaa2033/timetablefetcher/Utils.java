package com.iillyyaa2033.timetablefetcher;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import androidx.annotation.Keep;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class Utils {

    /**
     * Вызов диалога, в котором пользователь выбирает расписание
     */
    public static void showBonchSettings(final Context context, final OnTimetableSelectedListener listener) {

        final AlertDialog dialog = new AlertDialog.Builder(context).setCancelable(false).create();

        WebView view = getTimeTabledWebView(context, url -> {
            dialog.dismiss();
            listener.onTimeTableSelected(url);
        });

        dialog.setView(view);
        dialog.show();
    }

    @SuppressLint("SetJavaScriptEnabled")
    public static WebView getTimeTabledWebView(Context context, OnTimetableSelectedListener listener) {
        WebView view = new WebView(context);
        view.getSettings().setJavaScriptEnabled(true);

        String selector = "https://cabinet.sut.ru/raspisanie_all_new";

        WebViewClient webViewClient = new WebViewClient() {

            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {

                if (url.contains(".css")) {
                    return new WebResourceResponse(
                            "text/css",
                            "UTF-8",
                            context.getResources().openRawResource(R.raw.empty));
                } else {
                    return super.shouldInterceptRequest(view, url);
                }
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                if (url.startsWith("https://cabinet.sut.ru/raspisanie_all_new?")) {
                    listener.onTimeTableSelected(url);
                }
            }

            @Override
            public void onPageFinished(WebView web, String url) {
                web.loadUrl("javascript:(" +
                        "function(){" +
                        "document.getElementsByTagName(\"form\")[0].method=\"get\";" +
                        "})()");
            }


        };

        view.setWebViewClient(webViewClient);
        view.loadUrl(selector);

        return view;
    }

    public interface OnTimetableSelectedListener {
        void onTimeTableSelected(String url);
    }

    /**
     * Вычитываем известные сокращения из json'a в assets/
     *
     * @param context -
     * @return словарик "полное lowercase имя предмета" - "аббревиатура"
     */
    @Keep
    public static Map<String, String> getShortens(Context context) {

        HashMap<String, String> result = new HashMap<>();

        String rawJson = "";


        InputStream is = null;
        try {
            is = context.getAssets().open("shortens-sut.json");

            java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
            rawJson = s.hasNext() ? s.next() : "";

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (is != null) is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            Gson gson = new GsonBuilder().create();
            Type token = TypeToken.getParameterized(Map.class, String.class, String.class).getType();
            Map<String, String> map = gson.fromJson(rawJson, token);

            for (Map.Entry<String, String> entry : map.entrySet()) {
                result.put(entry.getKey().toLowerCase(), entry.getValue());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        return result;
    }
}
