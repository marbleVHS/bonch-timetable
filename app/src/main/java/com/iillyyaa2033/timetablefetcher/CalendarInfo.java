package com.iillyyaa2033.timetablefetcher;

import androidx.annotation.NonNull;

/**
 * Информация о календаре (который android'овский, из CalendarProvider)
 */
public class CalendarInfo {

    public final String id;
    public final String name;

    public CalendarInfo(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @NonNull
    @Override
    public String toString() {
        return name + " (#" + id + ")";
    }
}
