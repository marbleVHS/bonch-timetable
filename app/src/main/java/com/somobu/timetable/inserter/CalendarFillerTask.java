package com.somobu.timetable.inserter;

import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.CalendarContract;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.iillyyaa2033.timetablefetcher.Utils;
import com.somobu.timetable.parser.Event;
import com.iillyyaa2033.timetablefetcher.CalendarInfo;
import com.somobu.timetable.parser.DateSpan;
import com.somobu.timetable.parser.SutParser;

import java.util.ArrayList;
import java.util.TimeZone;

public class CalendarFillerTask extends Thread {

    private final DateSpan bounds;
    private final CalendarInfo selectedCalendar;
    private final String targetUrl;

    private Context context;
    private Handler handler;
    private AlertDialog d;

    public CalendarFillerTask(Context context, CalendarInfo calendar, DateSpan bounds, String target) {
        this.context = context;
        this.handler = new Handler();
        this.selectedCalendar = calendar;
        this.bounds = bounds;
        this.targetUrl = target;

        handler.post(this::onPreExecute);
    }

    protected void onPreExecute() {
        d = new AlertDialog.Builder(context).setMessage("Работаем...").create();
        d.setCancelable(false);
        d.show();
    }

    @Override
    public void run() {
        Exception caughtException = null;
        SutParser timetable = new SutParser(targetUrl, Utils.getShortens(context));

        try {
            timetable.parse();

            for (Event oldEvent : getCurrentEvents()) {
                deleteEvent(oldEvent.id);
            }

            for (Event ev : timetable.getEventsAt(bounds)) {
                insertOrUpdate(ev);
            }

        } catch (Exception e) {
            e.printStackTrace();
            caughtException = e;
        }

        final Exception finalException = caughtException;
        handler.post(() -> onPostExecute(finalException));
    }

    private void insertOrUpdate(Event event) {

        ContentValues values = new ContentValues();
        values.put(CalendarContract.Events.DTSTART, event.start);
        values.put(CalendarContract.Events.DTEND, event.start + event.duration);
        values.put(CalendarContract.Events.TITLE, event.title);
        values.put(CalendarContract.Events.DESCRIPTION, event.description);
        values.put(CalendarContract.Events.CALENDAR_ID, selectedCalendar.id);
        values.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().getID());
        values.put(CalendarContract.Events.EVENT_LOCATION, event.location);
        values.put(CalendarContract.Events.CUSTOM_APP_PACKAGE, context.getPackageName());

        Gson gson = new GsonBuilder().create();
        values.put(CalendarContract.Events.CUSTOM_APP_URI, "event://details?data=" + gson.toJson(event));

        boolean updated = false;

        for (Event oldEvent : getCurrentEvents()) {
            if (oldEvent.atSameDate(event)) {
                updateEvent(values, oldEvent.id);
                updated = true;
            }
        }

        if (!updated) insertEvent(values);
    }

    private ArrayList<Event> getCurrentEvents() {

        ArrayList<Event> list = new ArrayList<>();

        String[] projection = new String[]{
                CalendarContract.Events._ID,
                CalendarContract.Events.CALENDAR_ID,
                CalendarContract.Events.TITLE,
                CalendarContract.Events.DTSTART,
                CalendarContract.Events.CUSTOM_APP_PACKAGE
        };

        String selection = "( " + CalendarContract.Events.CALENDAR_ID + " == " + selectedCalendar.id
                + " ) AND ( " + CalendarContract.Events.DTSTART + " >= " + bounds.startMillis
                + " ) AND ( " + CalendarContract.Events.DTSTART + " <= " + bounds.endMillis
                + " ) AND ( " + CalendarContract.Events.CUSTOM_APP_PACKAGE + " == \"" + context.getPackageName() + "\""
                + " ) AND ( deleted != 1 )";

        Cursor cursor = context.getContentResolver().query(CalendarContract.Events.CONTENT_URI, projection, selection, null, null);

        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            do {
                list.add(new Event(cursor.getLong(0), cursor.getLong(1), cursor.getString(2), cursor.getLong(3)));
            } while (cursor.moveToNext());
            cursor.close();
        }

        return list;
    }

    private void insertEvent(ContentValues values) {
        context.getContentResolver().insert(CalendarContract.Events.CONTENT_URI, values);
    }

    private void updateEvent(ContentValues values, long replacementId) {
        context.getContentResolver().update(CalendarContract.Events.CONTENT_URI, values, CalendarContract.Events._ID + " == " + replacementId, null);
    }

    private void deleteEvent(long eventID) {
        Uri deleteUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, eventID);
        context.getContentResolver().delete(deleteUri, null, null);

    }

    protected void onPostExecute(Exception result) {
        d.dismiss();

        String message = "Запись в календарь завершена ";

        if (result == null) {
            message += "успешно";
        } else {
            message += "со следующей ошибкой:";
            message += "\n" + result.getMessage();
        }

        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("Окей", null)
                .show();
    }
}
