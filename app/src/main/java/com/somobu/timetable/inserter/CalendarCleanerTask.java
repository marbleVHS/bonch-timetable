package com.somobu.timetable.inserter;

import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.CalendarContract;

public class CalendarCleanerTask extends Thread {

    private Context context;
    private Handler handler;
    private AlertDialog d;

    public CalendarCleanerTask(Context context) {
        this.context = context;
        this.handler = new Handler();
        d = new AlertDialog.Builder(context).setMessage("Работаем").setCancelable(false).show();
    }

    @Override
    public void run() {

        Exception caughtException = null;

        try {
            String[] projection = new String[]{
                    CalendarContract.Events._ID,
                    CalendarContract.Events.CALENDAR_ID,
                    CalendarContract.Events.TITLE,
                    CalendarContract.Events.DTSTART,
                    CalendarContract.Events.CUSTOM_APP_PACKAGE
            };

            String selection = "(" + CalendarContract.Events.CUSTOM_APP_PACKAGE + " == \"" + context.getPackageName() + "\""
                    + " ) AND ( deleted != 1 )";

            Cursor cursor = context.getContentResolver().query(CalendarContract.Events.CONTENT_URI, projection, selection, null, null);

            if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
                do {
                    Uri deleteUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, cursor.getLong(0));
                    context.getContentResolver().delete(deleteUri, null, null);
                } while (cursor.moveToNext());
                cursor.close();
            }
        } catch (Exception ex) {
            caughtException = ex;
        }

        final Exception finalEx = caughtException;
        handler.post(() -> onPostExecute(finalEx));
    }

    protected void onPostExecute(Exception e) {
        d.dismiss();
        String message;

        if (e == null) {
            message = "Очистка календаря завершена ";
        } else {
            message = "Очистка календаря завершена с ошибкой:\n"
                    + e.getLocalizedMessage();
            e.printStackTrace();
        }

        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("Окей", null)
                .show();
    }
}
