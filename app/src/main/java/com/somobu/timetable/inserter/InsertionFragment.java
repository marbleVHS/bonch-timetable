package com.somobu.timetable.inserter;

import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.CalendarContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.iillyyaa2033.timetablefetcher.R;
import com.iillyyaa2033.timetablefetcher.CalendarInfo;
import com.somobu.timetable.IErrorShowing;
import com.somobu.timetable.MainActivity;
import com.somobu.timetable.parser.DateSpan;

import java.util.ArrayList;

public class InsertionFragment extends Fragment {

    private CalendarInfo selectedCalendar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_insertion, container, false);
        setupButton(root);

        root.findViewById(R.id.rq_perm).setOnClickListener(v -> {
            requestPermissions(new String[]{"android.permission.READ_CALENDAR", "android.permission.WRITE_CALENDAR"}, 243);
        });

        setup(root);

        return root;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, int[] grantResults) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            hidePermOverlay(null);
            setupCalendarSpinner(null);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setup(getView());
    }

    private void setup(@Nullable View root){
        if(root == null) return;;

        boolean permGranted = true;
        try {
            if (Build.VERSION.SDK_INT > 23) {
                if (getContext().checkSelfPermission("android.permission.WRITE_CALENDAR") != PackageManager.PERMISSION_GRANTED && getContext().checkSelfPermission("android.permission.READ_CALENDAR") != PackageManager.PERMISSION_GRANTED) {
                    permGranted = false;
                }
            }
        } catch (Exception e) {
            ((IErrorShowing) getActivity()).userErrorMessage(e);
        }

        if (permGranted) {
            hidePermOverlay(root);
            setupCalendarSpinner(root);
        } else {
            root.findViewById(R.id.no_permissions_warn).setVisibility(View.VISIBLE);
        }
    }

    private void setupButton(final View root) {
        root.findViewById(R.id.main_btn_start).setOnClickListener(v -> {
            DateSpan bounds = null;

            int item = ((Spinner) root.findViewById(R.id.main_spinner_bounds)).getSelectedItemPosition();
            switch (item) {
                case 0:
                    bounds = DateSpan.currentAndNextWeek();
                    break;
                case 1:
                    bounds = DateSpan.allAvailable();
                    break;
            }

            String savedQuery = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(MainActivity.SELECTED_QUERY_KEY, null);
            CalendarFillerTask cft = new CalendarFillerTask(getContext(), selectedCalendar, bounds, savedQuery);
            cft.start();
        });
    }

    private void hidePermOverlay(View root) {
        if (root == null) root = getView();
        if (root == null) return;

        root.findViewById(R.id.no_permissions_warn).setVisibility(View.GONE);
    }

    private void setupCalendarSpinner(View root) {
        if (root == null) root = getView();
        if (root == null) return;

        String rememberedCalendarId = PreferenceManager.getDefaultSharedPreferences(getContext()).getString("MAIN_UI_CAL_ID", null);

        Spinner sp = root.findViewById(R.id.main_spinner_calendar);

        ArrayList<CalendarInfo> calendars = new ArrayList<>();
        int savedSelectedCalendarId = -1;

        try {
            ContentResolver contentResolver = getActivity().getContentResolver();
            final Cursor cursor = contentResolver.query(Uri.parse("content://com.android.calendar/calendars"), new String[]{"_id", CalendarContract.Calendars.CALENDAR_DISPLAY_NAME}, null, null, null);

            int index = 0;

            if (cursor != null) {
                while (cursor.moveToNext()) {
                    final String _id = cursor.getString(0);
                    final String displayName = cursor.getString(1);
                    calendars.add(new CalendarInfo(_id, displayName));
                    if (_id.equals(rememberedCalendarId)) savedSelectedCalendarId = index;
                    index++;
                }

                cursor.close();
            }
        } catch (Exception e) {
            ((IErrorShowing) getActivity()).userErrorMessage(e);
        }

        if (calendars.size() > 0) selectedCalendar = calendars.get(0);

        final ArrayAdapter<CalendarInfo> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, calendars);
        sp.setAdapter(adapter);
        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCalendar = adapter.getItem(position);
                PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString("MAIN_UI_CAL_ID", "" + selectedCalendar.id).apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (savedSelectedCalendarId > -1) sp.setSelection(savedSelectedCalendarId);
    }
}
