package com.somobu.timetable;

import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.iillyyaa2033.timetablefetcher.R;

public class AboutFragment extends Fragment {

    private static final String s =
            "<b>Бонч.Расписание</b>"
                    + "<br/>Автор: Iria.Somobu"
                    + "<br/>При поддержке Valery Proshchenkov"
                    + "<br/><br/>Релиз: <a href=\"https://play.google.com/store/apps/details?id=com.iillyyaa2033.timetablefetcher\">в Google Play</a>"
                    + "<br/>Лицензия: <a href=\"https://gitlab.com/iria_somobu/bonch-timetable/blob/master/LICENSE.md\">GNU GPL v3</a>"
                    + "<br/>Исходный код: <a href=\"https://gitlab.com/iria_somobu/bonch-timetable/\">на gitlab.com</a>"
                    + "<br/><br/><b>Зависимости</b>"
                    + "<br/><a href=\"https://jsoup.org/\">JSOUP</a> (<a href=\"https://jsoup.org/license\">MIT license</a>)"
                    + "<br/><a href=\"https://android.googlesource.com/platform/frameworks/support\">AOSP</a> (<a href=\"http://www.apache.org/licenses/LICENSE-2.0\">Apache License 2.0</a>)";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_about, container, false);

        TextView text = root.findViewById(R.id.centered_text);

        text.setText(Html.fromHtml(s));

        // Make the textview clickable
        text.setMovementMethod(LinkMovementMethod.getInstance());

        return root;
    }
}
