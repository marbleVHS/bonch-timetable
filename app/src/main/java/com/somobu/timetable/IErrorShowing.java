package com.somobu.timetable;

public interface IErrorShowing {

    void userErrorMessage(Exception e);

}
