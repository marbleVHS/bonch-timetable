package com.somobu.timetable;

import android.app.AlertDialog;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;
import com.iillyyaa2033.timetablefetcher.R;
import com.iillyyaa2033.timetablefetcher.Utils;
import com.somobu.timetable.inserter.CalendarCleanerTask;

public class SettingsFragment extends PreferenceFragmentCompat {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.preferences);
        findPreference("sel_timetable").setOnPreferenceClickListener(preference -> {
            showSettings();
            return true;
        });
        findPreference("clear_calendars").setOnPreferenceClickListener(preference -> {
            showCleanCalendars();
            return true;
        });
        findPreference("got_bug").setOnPreferenceClickListener(preference -> {
            showErrorsDisclaimer();
            return true;
        });
    }

    void showSettings() {
        Utils.showBonchSettings(getActivity(), url -> {
            PreferenceManager.getDefaultSharedPreferences(getActivity())
                    .edit()
                    .putString(MainActivity.SELECTED_QUERY_KEY, url)
                    .apply();
        });
    }

    void showCleanCalendars() {
        new AlertDialog.Builder(getContext())
                .setMessage("Это удалит все созданные программой записи из всех календарей.\n\nПродолжить?")
                .setNegativeButton("Нет", null)
                .setPositiveButton("Ага", (dialog, which) -> new CalendarCleanerTask(getContext()).start())
                .show();
    }

    void showErrorsDisclaimer() {
        showInfoDialog("<b>Программа работает неправильно?</b>"
                + "<br/><br/>Вполне возможно. Я мог где-то ошибиться, или изменился алрогитм работы бончевского генератора расписания, или так сложились обстоятельства."
                + "<br/><br/>В любом случае, об ошибке можно сообщить в отзывах к этой программе на " +
                "<a href=\"https://play.google.com/store/apps/details?id=com.iillyyaa2033.timetablefetcher\">Google Play</a>, " +
                "или через тикет на <a href=\"https://gitlab.com/iria_somobu/bonch-timetable/-/issues\">gitlab.com</a>, " +
                "или же электронным письмом на <a href=\"mailto:foxaleksan@gmail.com\">мой ящик</a>.");
    }

    void showInfoDialog(String s) {
        AlertDialog d = new AlertDialog.Builder(getContext()).setMessage(Html.fromHtml(s)).setPositiveButton("Ясненько", null).create();
        d.show();

        TextView contents = d.findViewById(android.R.id.message);

        // Make the textview clickable. Must be called after show()
        contents.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
