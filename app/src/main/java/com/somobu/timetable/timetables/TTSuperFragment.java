package com.somobu.timetable.timetables;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.iillyyaa2033.timetablefetcher.R;
import com.somobu.timetable.parser.Event;
import com.somobu.timetable.parser.SutParser;

import java.util.List;

import javax.net.ssl.SSLPeerUnverifiedException;

public abstract class TTSuperFragment extends Fragment {


    void setLoading(View root) {
        root.findViewById(R.id.overlay).setVisibility(View.VISIBLE);

        TextView info = root.findViewById(R.id.overlay_text);
        info.setText("Обновляем расписание...");
    }

    void setError(View root, Exception ex) {
        root.findViewById(R.id.overlay).setVisibility(View.VISIBLE);

        TextView info = root.findViewById(R.id.overlay_text);

        if (ex instanceof SutParser.SemesterStartTimeUnknown) {
            info.setText("Дата начала нового семестра не заложена в эту версию программы. Следите за обновлениями");
        } else if (ex instanceof SSLPeerUnverifiedException) {
            info.setText("Если ты читаешь это сообщение, значит, скорее всего, боньч снова запорол SSL. " +
                    "Подобное уже случалось 31 авгруста 2020 года. Проблема не может быть исправлена из программы. " +
                    "Жди когда веб-мастера боньча все обратно починят." +
                    "\n\nОшибка: " + ex);
        } else {
            info.setText("Произошла трагическая ошибка: " + ex);
        }
    }

    public void setData(View root, List<Event> events) {
        root.findViewById(R.id.overlay).setVisibility(events.isEmpty() ? View.VISIBLE : View.GONE);

        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) root.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setRefreshing(false);

        TextView info = root.findViewById(R.id.overlay_text);

        if (events == null || events.size() == 0) {
            info.setText("На этой и следующей неделе пар не наблюдается");
        } else {
            ArrayAdapter<Event> adapter = new EventsAdapter(getContext(), events.toArray(new Event[0]));
            ListView listView = root.findViewById(R.id.list);
            listView.setAdapter(adapter);
        }
    }


}
