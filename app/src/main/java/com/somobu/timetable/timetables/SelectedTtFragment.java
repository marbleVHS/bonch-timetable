package com.somobu.timetable.timetables;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.iillyyaa2033.timetablefetcher.R;

public class SelectedTtFragment extends TTSuperFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Subscribe to updates
        gvm().get().observe(this, this::processResult);
    }

    @Override
    public void onResume() {
        super.onResume();

        // Update state
        gvm().loadData(getActivity());
    }

    TimetabledVM gvm() {
        return new ViewModelProvider(requireActivity()).get(TimetabledVM.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_events, container, false);

        new Handler().postDelayed(() -> {
            // Show current state
            processResult(gvm().get().getValue());
        }, 200);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        SwipeRefreshLayout swipeRefreshLayout = view.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.orange);

        swipeRefreshLayout.setOnRefreshListener(
                () -> gvm().loadData(getContext())
        );


        super.onViewCreated(view, savedInstanceState);
    }

    private void processResult(TimetabledVM.LoadingResult rz) {
        View root = getView();
        if (root == null) return;

        if (rz == null) setLoading(root);
        else if (rz.ex != null) setError(root, rz.ex);
        else setData(root, rz.list);
    }

}
