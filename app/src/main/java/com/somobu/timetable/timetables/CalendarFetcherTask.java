package com.somobu.timetable.timetables;

import android.content.Context;

import com.iillyyaa2033.timetablefetcher.Utils;
import com.somobu.timetable.parser.Event;
import com.somobu.timetable.parser.DateSpan;
import com.somobu.timetable.parser.SutParser;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class CalendarFetcherTask extends Thread {

    private Context context;
    private String targetUrl;
    private L l;

    public CalendarFetcherTask(Context context, String targetUrl) {
        this.context = context;
        this.targetUrl = targetUrl;
    }

    public CalendarFetcherTask l(L l) {
        this.l = l;
        return this;
    }

    @Override
    public void run() {
        String title = null;
        List<Event> list = null;
        Exception ex = null;

        try {
            SutParser timetable = new SutParser(targetUrl, Utils.getShortens(context));
            timetable.parse();
            title = timetable.semesterName;
            list = timetable.getEventsAt(DateSpan.currentAndNextWeek());

            Iterator<Event> i = list.iterator();
            while (i.hasNext()) {
                Event ev = i.next();
                if ((ev.start + ev.duration) < System.currentTimeMillis()) i.remove();
                else if (ev.start < System.currentTimeMillis()) ev.isOngoingNow = true;
            }

            Collections.sort(list);

        } catch (Exception e) {
            e.printStackTrace();
            ex = e;
        }

        if (l != null) l.onRz(title, list, ex);
    }

    public interface L {
        void onRz(String title, List<Event> list, Exception ex);
    }

}

