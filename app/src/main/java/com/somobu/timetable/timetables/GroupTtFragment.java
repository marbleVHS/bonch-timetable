package com.somobu.timetable.timetables;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.annotation.Nullable;
import com.iillyyaa2033.timetablefetcher.R;
import com.iillyyaa2033.timetablefetcher.Utils;
import com.somobu.timetable.parser.Event;

import java.util.List;

public class GroupTtFragment extends TTSuperFragment implements Utils.OnTimetableSelectedListener {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_group_tt, container, false);

        LinearLayout rt = root.findViewById(R.id.root);
        rt.addView(Utils.getTimeTabledWebView(inflater.getContext(), this));

        return root;
    }

    @Override
    public void onTimeTableSelected(String url) {
        View root = reinflate();
        Context context = getActivity();
        if (context == null || root == null) return;

        setLoading(root);

        Handler h = new Handler(context.getMainLooper());
        new CalendarFetcherTask(context, url).l((title, list, ex) -> h.post(() -> onLoadingResult(list, ex))).start();
    }

    private void onLoadingResult(List<Event> pairs, Exception ex) {
        View root = reinflate();
        Context context = getActivity();
        if (context == null || root == null) return;

        if (ex != null) setError(root, ex);
        else setData(root, pairs);
    }

    private View reinflate() {
        Context context = getActivity();
        if (context == null) return null;

        View root = LayoutInflater.from(context).inflate(R.layout.fragment_events, null, false);
        LinearLayout ll = (getView().findViewById(R.id.root));
        ll.removeAllViews();
        ll.addView(root);
        getView().findViewById(R.id.swipe_refresh).setEnabled(false);

        return root;
    }
}
