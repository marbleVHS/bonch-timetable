package com.somobu.timetable.timetables;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.browser.customtabs.CustomTabsIntent;
import com.iillyyaa2033.timetablefetcher.R;
import com.somobu.timetable.parser.Event;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressLint("SimpleDateFormat")
public class EventsAdapter extends ArrayAdapter<Event> {

    DateFormat dateFormat = new SimpleDateFormat("dd.MM, EEEE");
    DateFormat timeFormat = new SimpleDateFormat("HH:mm");


    public EventsAdapter(@NonNull Context context, @NonNull Event[] objects) {
        super(context, android.R.layout.simple_list_item_1, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_calendarevent, parent, false);
        }

        Event prev = null;
        if (position > 0) prev = getItem(position - 1);
        Event curr = getItem(position);
        boolean notSameDate = prev == null || !prev.atSameDay(curr);

        convertView.findViewById(R.id.event_bg).setBackgroundColor(curr.isOngoingNow ? Color.LTGRAY : Color.TRANSPARENT);

        TextView divider = convertView.findViewById(R.id.divider);
        divider.setText(dateFormat.format(new Date(curr.start)));
        divider.setVisibility(notSameDate ? View.VISIBLE : View.GONE);

        TextView title = convertView.findViewById(R.id.title);
        title.setText(curr.title);

        TextView dateloc = convertView.findViewById(R.id.date_loc);
        setupDateloc(dateloc, curr);

        TextView descr = convertView.findViewById(R.id.descr);
        descr.setText(curr.shortDescr);

        return convertView;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }


    void setupDateloc(TextView dateloc, Event curr) {
        String time = timeFormat.format(new Date(curr.start)) + " - " + timeFormat.format(new Date(curr.start + curr.duration));

        String loc;
        if (curr.location.contains("/") && curr.location.matches("[0-9/\\-].*")) {
            String[] split = curr.location.split("/");

            String arg = "k" + split[split.length - 1] + "-";
            arg += split[0];
            if (split.length == 3) arg += "a" + split[2];

            loc = "<a href=\"https://nav.sut.ru/?cab=" + arg + "\">" + curr.location + "</b>";
        } else {
            loc = curr.location;
        }

        String pairPrefix = "";
        if (!curr.pair.trim().isEmpty()) {
            pairPrefix = curr.pair.trim() + " | ";
        }

        String dl = pairPrefix + time + " | " + loc;
        setTextViewHTML(dateloc, dl);
    }

    protected void setTextViewHTML(TextView text, String html) {
        CharSequence sequence = Html.fromHtml(html);
        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
        URLSpan[] urls = strBuilder.getSpans(0, sequence.length(), URLSpan.class);
        for (URLSpan span : urls) {
            makeLinkClickable(strBuilder, span);
        }
        text.setText(strBuilder);
        text.setMovementMethod(LinkMovementMethod.getInstance());
    }

    protected void makeLinkClickable(SpannableStringBuilder strBuilder, final URLSpan span) {
        int start = strBuilder.getSpanStart(span);
        int end = strBuilder.getSpanEnd(span);
        int flags = strBuilder.getSpanFlags(span);
        ClickableSpan clickable = new ClickableSpan() {
            public void onClick(View view) {
                String url = span.getURL();
                launchInCustomTab(url);
            }
        };
        strBuilder.setSpan(clickable, start, end, flags);
        strBuilder.removeSpan(span);
    }

    private void launchInCustomTab(String url) {

        Context context = getContext();
        if (context == null) return;

        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;

        Uri uri = Uri.parse(url);

        try {
            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
            builder.addDefaultShareMenuItem();
            builder.enableUrlBarHiding();
            builder.setExitAnimations(context, android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            builder.setStartAnimations(context, android.R.anim.slide_in_left, android.R.anim.slide_out_right);

            CustomTabsIntent customTabsIntent = builder.build();
            customTabsIntent.launchUrl(context, uri);
        } catch (ActivityNotFoundException e) {

            try {
                context.startActivity(new Intent(Intent.ACTION_VIEW, uri));
            } catch (ActivityNotFoundException e1) {
                Toast.makeText(context, "Cannot pick app to view " + url, Toast.LENGTH_LONG).show();
                e1.printStackTrace();
            }
        }
    }

}
