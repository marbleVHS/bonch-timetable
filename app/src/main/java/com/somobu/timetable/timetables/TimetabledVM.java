package com.somobu.timetable.timetables;

import android.content.Context;
import android.os.Handler;
import android.preference.PreferenceManager;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.somobu.timetable.parser.Event;

import java.util.List;

import static com.somobu.timetable.MainActivity.SELECTED_QUERY_KEY;

public class TimetabledVM extends ViewModel {

    private final MutableLiveData<LoadingResult> userLiveData = new MutableLiveData<>();

    private String lastSavedQuery = "";

    public MutableLiveData<LoadingResult> get() {
        return userLiveData;
    }

    public void loadData(Context ctx) {
        String savedQuery = PreferenceManager.getDefaultSharedPreferences(ctx).getString(SELECTED_QUERY_KEY, null);
        if (savedQuery == null) return;

        if (!lastSavedQuery.equals(savedQuery)) {
            userLiveData.postValue(null);
            lastSavedQuery = savedQuery;
        }

        Handler h = new Handler(ctx.getMainLooper());

        new CalendarFetcherTask(ctx, savedQuery)
                .l((title, list, ex) -> h.post(() -> {
                    LoadingResult rz = new LoadingResult();
                    rz.list = list;
                    rz.ex = ex;
                    userLiveData.postValue(rz);
                })).start();
    }

    public static class LoadingResult {

        public List<Event> list = null;
        public Exception ex = null;

    }

}
